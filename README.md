# Microservice architecture
List of patterns and solutions in software development world.

### Service discovery
Let's imagine, you are writing some code that invokes a service that has a REST API.

### Messaging

* point-to-point - channel delivers a message to exactly one of the consumers that is reading from the channel. 
* publish-subscribe - channel delivers each message to all of the attached consumers.

### Polling messages
This is also known as a synchronous receiver, because the receiver thread blocks until a message is received. We call it a Polling Consumer because the receiver polls for a message, processes it, then polls for another. 

### Kafka
Kafka supports publish-subscribe pattern, which implemented via topics. Kafka topics contains of sharded (partitioned) channels.
Kafka guarantees, that each event has correct order inside partition.

<a href="https://ibb.co/1RVw9hF"><img src="https://i.ibb.co/L9w2CbV/image.png" alt="image" border="0"></a>

### Kafka Producer
In general, the messaging approach (brokers usage) helps with discovering of physical address. 
If we are talking about cluster with multiple brokers, then it's really important. 
Producers don't care about the physical address of the cluster broker, because producers automatically know it.

<a href="https://ibb.co/4tQwgnN"><img src="https://i.ibb.co/V9K0pcB/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/sKBnD6m"><img src="https://i.ibb.co/tqdgNxm/image.png" alt="image" border="0"></a>

### Kafka Consumer

<a href="https://ibb.co/zVXWp50"><img src="https://i.ibb.co/0yCpTXd/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/s9tpr3F"><img src="https://i.ibb.co/WvsN1Kc/image.png" alt="image" border="0"></a>

### Kafka Cluster

<a href="https://ibb.co/0JPPzW2"><img src="https://i.ibb.co/rynnXL0/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/kyJWxC8"><img src="https://i.ibb.co/9vhCN5T/image.png" alt="image" border="0"></a>

<a href="https://ibb.co/wNcsc5d"><img src="https://i.ibb.co/bgbJbc6/image.png" alt="image" border="0"></a>

### Saga pattern
Distributed transactions are very complicated in terms of _microservice architecture_ (more convenient for _monolithic_). 

__Sagas__  are  mechanisms  to  maintain  data  consistency  in  a  _microservice  architecturewithout_ having to use distributed transactions. You define a __saga__ for each system command  that  needs  to  update  data  in  multiple  services.  A  saga  is  a  __sequence  of  local transactions__.  Each  __local  transaction__  updates  data  within  a  single  service  using  the familiar ACID transaction frameworks and libraries.

The first transaction in a __saga__ is initiated by an external request corresponding to the system operation, and then each subsequent step is triggered by the completion of the previous one. 

Example: __saga for order creation__

>The saga’s first local transaction is initiated by the external request tocreate an order. The other five local transactions are each triggered by completion ofthe previous one.

<a href="https://ibb.co/jwh1qzy"><img src="https://i.ibb.co/rpk1q3x/image.png" alt="image" border="0"></a>

### Saga types
There are two yupes of implementing saga:
* `Events/Choreography` - When there is __no central coordination__, each service produces and listen to other service’s events and decides if an action should be taken or not.

* `Command/Orchestration` - When a __coordinator service__ is responsible for centralizing the saga’s decision making and sequencing business logic. 

__Events/Choreography:__

In the _Events/Choreography_ approach, the _first service_ executes a transaction and then publishes an event. This event is listened by one or more services which execute local transactions and publish (or not) new events. The distributed transaction ends when the _last service_ executes its local transaction and does not publish any events or the event published is not heard by any of the saga’s participants.

<a href="https://ibb.co/tbwGRXh"><img src="https://i.ibb.co/D7PJdpb/image.png" alt="image" border="0"></a>

1. Order Service saves a new order, set the state as pending and publish an event called `ORDER_CREATED_EVENT`.
2. The Payment Service listens to `ORDER_CREATED_EVENT`, charge the client and publish the event `BILLED_ORDER_EVENT`.
3. The Stock Service listens to `BILLED_ORDER_EVENT`, update the stock, prepare the products bought in the order and publish `ORDER_PREPARED_EVENT`.
4. Delivery Service listens to `ORDER_PREPARED_EVENT` and then pick up and deliver the product. At the end, it publishes an `ORDER_DELIVERED_EVENT`.
5. Finally, Order Service listens to `ORDER_DELIVERED_EVENT` and set the state of the order as concluded.

__Command/Orchestration:__

In the _orchestration approach_, we define a new service with the sole responsibility of telling each participant what to do and when. The saga pattern orchestrator communicates with each service in a _command/reply_ style telling them what operation should be performed.

<a href="https://ibb.co/R42rhNm"><img src="https://i.ibb.co/k6Kb3Jf/image.png" alt="image" border="0"></a>

1. Order Service saves a pending order and asks Order Saga Orchestrator (`OSO`) to start a create order transaction.
1. `OSO` sends an Execute Payment command to Payment Service, and it replies with a Payment Executed message
1. `OSO` sends a Prepare Order command to Stock Service, and it replies with an Order Prepared message
1. `OSO` sends a Deliver Order command to Delivery Service, and it replies with an Order Delivered message

In the case above, `Order Saga Orchestrator` knows what is the flow needed to execute a “create order” transaction. If anything fails, it is also responsible for coordinating the rollback by sending commands to each participant to undo the previous operation.

A standard way to model a saga orchestrator is a State Machine where each transformation corresponds to a command or message. State machines are an excellent pattern to structure a well-defined behavior as they are easy to implement and particularly great for testing.

### Saga's transaction rollback 
A  great  feature  of  _traditional  ACID  transactions_  is  that  the  business  logic  can  easily roll back a transaction if it detects the violation of a business rule. It executes a `ROLL-BACK` statement, and the database undoes all the changes made so far. Unfortunately, sagas can’t be automatically rolled back, because each step commits its changes to the local  database.

This  means,  for  example,  that  if  the  authorization  of  the  credit  cardfails in the fourth step of the `Create Order Saga`, the application __must explicitly undo the changes made by the first three steps__. 

### Command Query Responsibility Segregation (CQRS)
How to implement a query that retrieves data from multiple services (aggregates) in a microservice architecture?
Just define a _view database_, which is a read-only replica that is designed to support that query (aggregation). The application keeps the replica up to data by subscribing to _domain events_ published by the service that own the data.

Output of CQRS:
* read side __available in case of failover__
* __scaling independent__
* realizing __consistency__ through independent systems

<a href="https://ibb.co/3CHrwBN"><img src="https://i.ibb.co/x7QCyJj/image.png" alt="image" border="0"></a><br /><a target='_blank' href='https://imgbb.com/'></a><br />

### CAP theorem
The CAP Theorem is a fundamental theorem in distributed systems that states any distributed system can have at most two of the following three properties.

* Consistency
* Availability
* Partition tolerance

The __CAP theorem states that a distributed system cannot simultaneously be consistent, available, and partition tolerant__. Sounds simple enough, but what does it mean to be consistent? available? partition tolerant? Heck, what exactly do you even mean by a distributed system?

In this section, we'll introduce a simple distributed system and explain what it means for that system to be available, consistent, and partition tolerant. 

#### A distributed system

<a href="https://ibb.co/k6s6rD1"><img src="https://i.ibb.co/7NdNTp4/image.png" alt="image" border="0"></a>


